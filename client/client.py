from flask import Flask
import requests
import os
import time
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

SERVER_ENDPOINT=os.getenv('SERVER_URI')

app = Flask(__name__)

@app.route("/health")
def get_datas():
    
    resource = Resource(attributes={
        "service.name": "service"
    })    
    
    trace.set_tracer_provider(TracerProvider(resource=resource))
    tracer = trace.get_tracer(__name__)

    otlp_exporter = OTLPSpanExporter(endpoint="otel-collector:4317", insecure=True)

    span_processor = BatchSpanProcessor(otlp_exporter)

    trace.get_tracer_provider().add_span_processor(span_processor)

    with tracer.start_as_current_span("foo"):
        print("Hello world!")

    return "lol"

@app.route("/data")
def get_data():
    while True:
        req=requests.get(SERVER_ENDPOINT+"/user")
        time.sleep(1)
    return ""

if __name__=='__main__':
    app.run(host='0.0.0.0',port=10000)
