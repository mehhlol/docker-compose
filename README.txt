context
example project of podman and podman-compose of containers interaction
will install 4 podman container
1 backend api
2 client
3 otlp
4 jaeger

Requirements
podman
podman-compose

setup
podman-compose -f compose.yaml up // to launch
podman-compose down // to stop

results
localhost:10000/health to trigger a trace
localhost:16686 to check trace on jaeger interface
